import { useState, useEffect, useContext } from 'react';
import { Loading } from '../components/Loading';
import { useNavigate } from "react-router-dom";
import { AuthContext } from '../context/AuthContext';
import { Navbar } from '../components/Navbar';
import { Header } from '../components/Header';
import env from "react-dotenv";

export const Add = () => {

  const API_URL = env.API_URL;
  const API_PORT = env.API_PORT;

  const navigate = useNavigate();
  const { logged, loading } = useContext(AuthContext);

  useEffect(() => {
    if (!loading) {
      if (!logged) {
        navigate('/login');
      }
    }
  }, [logged, loading, navigate]);

  const [types, setTypes] = useState([]);
  const [usecases, setUsecases] = useState([]);

  useEffect(() => {
    fetch(API_URL + ':' + API_PORT + '/api/v1/type/list')
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          console.log('Error:', data.error);
          return;
        }
        setTypes(data.types);
      })
      .catch((error) => {
        console.log('Error:', error);
        // alert(error);
      });

    fetch(API_URL + ':' + API_PORT + '/api/v1/usecase/list')
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          console.log('Error:', data.error);
          return;
        }
        setUsecases(data.usecases);
      }
      )
      .catch((error) => {
        console.log('Error:', error);
        // alert(error);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    const email = data.get('email');
    const usecase = data.get('usecase');
    const type = data.get('type');
    const destroy_date = data.get('destroy_date');
    const numberOfNodes = data.get('number_of_nodes');

    fetch(API_URL + ':' + API_PORT + '/api/v1/node/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: new URLSearchParams({
        email: email,
        usecase: usecase,
        type: type,
        destroy_date: destroy_date,
        numberOfNodes: numberOfNodes
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          alert(data.error);
          return;
        }
        console.log('Success:', data);
        alert(data.message);
      })
      .catch((error) => {
        console.log('Error:', error);
        alert(error);
      });
  }



  return (
    <>
      <Loading />
      <Navbar />
      <div className='view'>
        <Header page="Add Node" />
        <main>
          <h4><b>Enter your new node information below</b></h4>
          <form className='node_form' onSubmit={handleSubmit}>
            <label htmlFor='email'>
              Email
            </label>

            <input type="text" name="email" placeholder='Email' />
            <label htmlFor='usecase'>
              Use case
            </label>

            <select name="usecase">
              {
                usecases.map((usecase, index) => {
                  return <option key={index} value={usecase.code}>{`${usecase.code} - ${usecase.name}`}</option>
                })
              }
            </select>
            <label htmlFor='type'>
              Type
            </label>

            <select name="type">
              {
                types.map((type, index) => {
                  return <option key={index} value={type}>{type}</option>
                })
              }
            </select>
            <label htmlFor='destroy_date'>
              Duration
            </label>

            <select name="destroy_date">
              <option value="1">1 day</option>
              <option value="2">2 days</option>
              <option value="3">3 days</option>
              <option value="4">4 days</option>
              <option value="5">5 days</option>
              <option value="6">6 days</option>
              <option value="7">7 days</option>
            </select>

            <label htmlFor='number_of_nodes'>
              Number of nodes
            </label>

            <select name="number_of_nodes">
              <option value="1">1 node</option>
              <option value="2">2 nodes</option>
              <option value="3">3 nodes</option>
              <option value="4">4 nodes</option>
              <option value="5">5 nodes</option>
            </select>
            <input type="submit" value="Submit" />
          </form>
        </main>
      </div>
    </>
  );
}
