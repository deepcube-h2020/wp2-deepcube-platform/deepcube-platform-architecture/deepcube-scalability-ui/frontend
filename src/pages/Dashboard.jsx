import { useState, useEffect, useContext, useRef } from 'react';
import { Loading } from '../components/Loading';
import { useNavigate } from "react-router-dom";
import { AuthContext } from '../context/AuthContext';
import { NodesContext } from '../context/NodesContext';
import { Navbar } from '../components/Navbar';
import { Header } from '../components/Header';
import { LineChart } from '../components/LineChart';
import { DonutChart } from '../components/DonutChart';
import env from "react-dotenv";

import {
    Chart as ChartJS, ArcElement, Tooltip, Legend, CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title
} from 'chart.js';
import { Doughnut, Line } from 'react-chartjs-2';
ChartJS.register(ArcElement, Tooltip, Legend, CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,);

// import faker from 'faker';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);


export const Dashboard = () => {

    const API_URL = env.API_URL;
    const API_PORT = env.API_PORT;

    const navigate = useNavigate();
    const { logged, loading } = useContext(AuthContext);
    const { nodes, getNodes } = useContext(NodesContext);
  
    useEffect(() => {
      if (!loading) {
        if (!logged) {
          navigate('/login');
        }
      }
    }, [logged, loading, navigate]);

    const [filterStatus, setFilterStatus] = useState('');
    const [filterType, setFilterType] = useState('');
    const [sortDate, setSortDate] = useState('');
    const [search, setSearch] = useState('');

    const [uuidPopup, setUuidPopup] = useState(null);
    const popupRef = useRef();
    const popupContentRef = useRef();
    const btnDeleteRef = useRef();

    const popupRefUpdate = useRef();
    const popupContentRefUpdate = useRef();

    const handleClosePopupUpdate = () => {
        popupRefUpdate.current.classList.remove('active');
        popupRefUpdate.current.classList.add('close');
        setUuidPopup(null);
        setTimeout(() => {
            popupRefUpdate.current.classList.remove('close');
        }, 500);
    }

    const handleOpenPopupUpdate = () => {
        popupRefUpdate.current.classList.add('active');
    }

    const handleClosePopup = () => {
        popupRef.current.classList.remove('active');
        popupRef.current.classList.add('close');
        setUuidPopup(null);
        setTimeout(() => {
            popupRef.current.classList.remove('close');
        }, 500);
    }

    const handleOpenPopup = () => {
        popupRef.current.classList.add('active');
    }

    const tbodyRef = useRef();
    const [tbodyEmpty, setTbodyEmpty] = useState(false);

    useEffect(() => {
        if (tbodyRef.current.children[0]?.id !== 'notfound') {
            if (tbodyRef.current.children.length === 0) {
                setTbodyEmpty(true);
            } else {
                setTbodyEmpty(false);
            }
        }
    }, [filterStatus, filterType, sortDate, search]);

    useEffect(() => {
        getNodes();

        window.addEventListener('click', (event) => {
            if (event.target === popupRef.current) {
                handleClosePopup();
            }
            if (event.target === popupRefUpdate.current) {
                handleClosePopupUpdate();
            }
        });

        return () => {
            window.removeEventListener('click', () => { });
        }
    }, []);

    const handleDelete = (uuid) => {
        fetch(API_URL + ':' + API_PORT + '/api/v1/node/pendingdestroy', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: new URLSearchParams({
                uuid: uuid
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                console.log('Success:', data);
                alert(data.message);
                getNodes();
            })
            .catch((error) => {
                console.log('Error:', error);
                alert(error);
            });
    }

    const handleSubmitDestroy = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        const verify = data.get('verify');

        if (verify !== 'destroy') {
            alert('You must type "destroy" to confirm.');
            return;
        }

        handleDelete(uuidPopup);
        handleClosePopup();
    }

    const statusIcons = [
        {
            status: 'pendingprovisioning',
            icon: '<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M11.0055 2H12.9945C14.3805 1.99999 15.4828 1.99999 16.3716 2.0738C17.2819 2.14939 18.0575 2.30755 18.7658 2.67552C19.8617 3.24477 20.7552 4.13829 21.3245 5.23415C21.6925 5.94253 21.8506 6.71811 21.9262 7.62839C22 8.51722 22 9.6195 22 11.0055V12.9945C22 14.3805 22 15.4828 21.9262 16.3716C21.8506 17.2819 21.6925 18.0575 21.3245 18.7658C20.7552 19.8617 19.8617 20.7552 18.7658 21.3245C18.0575 21.6925 17.2819 21.8506 16.3716 21.9262C15.4828 22 14.3805 22 12.9945 22H11.0055C9.6195 22 8.51722 22 7.62839 21.9262C6.71811 21.8506 5.94253 21.6925 5.23415 21.3245C4.13829 20.7552 3.24477 19.8617 2.67552 18.7658C2.30755 18.0575 2.14939 17.2819 2.0738 16.3716C1.99999 15.4828 1.99999 14.3805 2 12.9945V11.0055C1.99999 9.61949 1.99999 8.51721 2.0738 7.62839C2.14939 6.71811 2.30755 5.94253 2.67552 5.23415C3.24477 4.13829 4.13829 3.24477 5.23415 2.67552C5.94253 2.30755 6.71811 2.14939 7.62839 2.0738C8.51721 1.99999 9.61949 1.99999 11.0055 2ZM7.79391 4.06694C7.00955 4.13207 6.53142 4.25538 6.1561 4.45035C5.42553 4.82985 4.82985 5.42553 4.45035 6.1561C4.25538 6.53142 4.13207 7.00955 4.06694 7.79391C4.0008 8.59025 4 9.60949 4 11.05V12.95C4 14.3905 4.0008 15.4097 4.06694 16.2061C4.13207 16.9905 4.25538 17.4686 4.45035 17.8439C4.82985 18.5745 5.42553 19.1702 6.1561 19.5497C6.53142 19.7446 7.00955 19.8679 7.79391 19.9331C8.59025 19.9992 9.60949 20 11.05 20H12.95C14.3905 20 15.4097 19.9992 16.2061 19.9331C16.9905 19.8679 17.4686 19.7446 17.8439 19.5497C18.5745 19.1702 19.1702 18.5745 19.5497 17.8439C19.7446 17.4686 19.8679 16.9905 19.9331 16.2061C19.9992 15.4097 20 14.3905 20 12.95V11.05C20 9.60949 19.9992 8.59025 19.9331 7.79391C19.8679 7.00955 19.7446 6.53142 19.5497 6.1561C19.1702 5.42553 18.5745 4.82985 17.8439 4.45035C17.4686 4.25538 16.9905 4.13207 16.2061 4.06694C15.4097 4.0008 14.3905 4 12.95 4H11.05C9.60949 4 8.59025 4.0008 7.79391 4.06694ZM11.8284 6.75736C12.3807 6.75736 12.8284 7.20507 12.8284 7.75736V12.7245L16.3553 14.0653C16.8716 14.2615 17.131 14.8391 16.9347 15.3553C16.7385 15.8716 16.1609 16.131 15.6447 15.9347L11.4731 14.349C11.085 14.2014 10.8284 13.8294 10.8284 13.4142V7.75736C10.8284 7.20507 11.2761 6.75736 11.8284 6.75736Z" fill="orange"/></svg>'
        },
        {
            status: 'provisioning',
            icon: '<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 13C16.5523 13 17 12.5523 17 12C17 11.4477 16.5523 11 16 11C15.4477 11 15 11.4477 15 12C15 12.5523 15.4477 13 16 13Z" fill="yellow"/><path d="M13 12C13 12.5523 12.5523 13 12 13C11.4477 13 11 12.5523 11 12C11 11.4477 11.4477 11 12 11C12.5523 11 13 11.4477 13 12Z" fill="yellow"/><path d="M8 13C8.55228 13 9 12.5523 9 12C9 11.4477 8.55228 11 8 11C7.44772 11 7 11.4477 7 12C7 12.5523 7.44772 13 8 13Z" fill="yellow"/><path fillRule="evenodd" clipRule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12Z" fill="yellow"/></svg>'
        },
        {
            status: 'active',
            icon: '<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M16.7071 7.29289C17.0976 7.68342 17.0976 8.31658 16.7071 8.70711L8.41421 17C7.63316 17.7811 6.36683 17.781 5.58579 17L6.29289 16.2929L5.58579 17L2.29289 13.7071C1.90237 13.3166 1.90237 12.6834 2.29289 12.2929C2.68342 11.9024 3.31658 11.9024 3.70711 12.2929L7 15.5858L15.2929 7.29289C15.6834 6.90237 16.3166 6.90237 16.7071 7.29289ZM21.7071 7.29289C22.0976 7.68342 22.0976 8.31658 21.7071 8.70711L12.7071 17.7071C12.3166 18.0976 11.6834 18.0976 11.2929 17.7071C10.9024 17.3166 10.9024 16.6834 11.2929 16.2929L20.2929 7.29289C20.6834 6.90237 21.3166 6.90237 21.7071 7.29289Z" fill="var(--color-green)"/></svg>'
        },
        {
            status: 'pendingdestroy',
            icon: '<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 11C7.44772 11 7 11.4477 7 12C7 12.5523 7.44772 13 8 13H16C16.5523 13 17 12.5523 17 12C17 11.4477 16.5523 11 16 11H8Z" fill="orange"/><path fillRule="evenodd" clipRule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12Z" fill="orange"/></svg>'
        },
        {
            status: 'destroyed',
            icon: '<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.70711 8.29289C9.31658 7.90237 8.68342 7.90237 8.29289 8.29289C7.90237 8.68342 7.90237 9.31658 8.29289 9.70711L10.5858 12L8.29289 14.2929C7.90237 14.6834 7.90237 15.3166 8.29289 15.7071C8.68342 16.0976 9.31658 16.0976 9.70711 15.7071L12 13.4142L14.2929 15.7071C14.6834 16.0976 15.3166 16.0976 15.7071 15.7071C16.0976 15.3166 16.0976 14.6834 15.7071 14.2929L13.4142 12L15.7071 9.70711C16.0976 9.31658 16.0976 8.68342 15.7071 8.29289C15.3166 7.90237 14.6834 7.90237 14.2929 8.29289L12 10.5858L9.70711 8.29289Z" fill="var(--color-red)"/><path fillRule="evenodd" clipRule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12Z" fill="var(--color-red)"/></svg>'
        }
    ]

    const handleSubmitUpdate = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        const duration = data.get('duration');

        fetch(API_URL + ':' + API_PORT + '/api/v1/node/update', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: new URLSearchParams({
                uuid: uuidPopup,
                duration: duration
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                console.log('Success:', data);
                alert(data.message);
                getNodes();
                handleClosePopupUpdate();
            })
            .catch((error) => {
                console.log('Error:', error);
                alert(error);
            });
    }

    return (
        <>
            <Loading />
            <Navbar />
            <div className='view'>
                <Header page="Dashboard" />
                <div className='stats'>
                    <div className='status-overview'>
                        <h5><b>Nodes per Status</b></h5>
                        <div className='status-data'>
                            <svg width="30px" height="30px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M16.7071 7.29289C17.0976 7.68342 17.0976 8.31658 16.7071 8.70711L8.41421 17C7.63316 17.7811 6.36683 17.781 5.58579 17L6.29289 16.2929L5.58579 17L2.29289 13.7071C1.90237 13.3166 1.90237 12.6834 2.29289 12.2929C2.68342 11.9024 3.31658 11.9024 3.70711 12.2929L7 15.5858L15.2929 7.29289C15.6834 6.90237 16.3166 6.90237 16.7071 7.29289ZM21.7071 7.29289C22.0976 7.68342 22.0976 8.31658 21.7071 8.70711L12.7071 17.7071C12.3166 18.0976 11.6834 18.0976 11.2929 17.7071C10.9024 17.3166 10.9024 16.6834 11.2929 16.2929L20.2929 7.29289C20.6834 6.90237 21.3166 6.90237 21.7071 7.29289Z" fill="var(--color-green)" /></svg>
                            <div className=''>
                                <span>Active</span>
                                <br />
                                <h5>
                                    {nodes && nodes.filter(node => node.status === 'active').length}
                                </h5>

                            </div>
                        </div>
                        {/* <div className='divider'></div> */}
                        <div className='status-data'>
                            <svg width="30px" height="30px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M11.0055 2H12.9945C14.3805 1.99999 15.4828 1.99999 16.3716 2.0738C17.2819 2.14939 18.0575 2.30755 18.7658 2.67552C19.8617 3.24477 20.7552 4.13829 21.3245 5.23415C21.6925 5.94253 21.8506 6.71811 21.9262 7.62839C22 8.51722 22 9.6195 22 11.0055V12.9945C22 14.3805 22 15.4828 21.9262 16.3716C21.8506 17.2819 21.6925 18.0575 21.3245 18.7658C20.7552 19.8617 19.8617 20.7552 18.7658 21.3245C18.0575 21.6925 17.2819 21.8506 16.3716 21.9262C15.4828 22 14.3805 22 12.9945 22H11.0055C9.6195 22 8.51722 22 7.62839 21.9262C6.71811 21.8506 5.94253 21.6925 5.23415 21.3245C4.13829 20.7552 3.24477 19.8617 2.67552 18.7658C2.30755 18.0575 2.14939 17.2819 2.0738 16.3716C1.99999 15.4828 1.99999 14.3805 2 12.9945V11.0055C1.99999 9.61949 1.99999 8.51721 2.0738 7.62839C2.14939 6.71811 2.30755 5.94253 2.67552 5.23415C3.24477 4.13829 4.13829 3.24477 5.23415 2.67552C5.94253 2.30755 6.71811 2.14939 7.62839 2.0738C8.51721 1.99999 9.61949 1.99999 11.0055 2ZM7.79391 4.06694C7.00955 4.13207 6.53142 4.25538 6.1561 4.45035C5.42553 4.82985 4.82985 5.42553 4.45035 6.1561C4.25538 6.53142 4.13207 7.00955 4.06694 7.79391C4.0008 8.59025 4 9.60949 4 11.05V12.95C4 14.3905 4.0008 15.4097 4.06694 16.2061C4.13207 16.9905 4.25538 17.4686 4.45035 17.8439C4.82985 18.5745 5.42553 19.1702 6.1561 19.5497C6.53142 19.7446 7.00955 19.8679 7.79391 19.9331C8.59025 19.9992 9.60949 20 11.05 20H12.95C14.3905 20 15.4097 19.9992 16.2061 19.9331C16.9905 19.8679 17.4686 19.7446 17.8439 19.5497C18.5745 19.1702 19.1702 18.5745 19.5497 17.8439C19.7446 17.4686 19.8679 16.9905 19.9331 16.2061C19.9992 15.4097 20 14.3905 20 12.95V11.05C20 9.60949 19.9992 8.59025 19.9331 7.79391C19.8679 7.00955 19.7446 6.53142 19.5497 6.1561C19.1702 5.42553 18.5745 4.82985 17.8439 4.45035C17.4686 4.25538 16.9905 4.13207 16.2061 4.06694C15.4097 4.0008 14.3905 4 12.95 4H11.05C9.60949 4 8.59025 4.0008 7.79391 4.06694ZM11.8284 6.75736C12.3807 6.75736 12.8284 7.20507 12.8284 7.75736V12.7245L16.3553 14.0653C16.8716 14.2615 17.131 14.8391 16.9347 15.3553C16.7385 15.8716 16.1609 16.131 15.6447 15.9347L11.4731 14.349C11.085 14.2014 10.8284 13.8294 10.8284 13.4142V7.75736C10.8284 7.20507 11.2761 6.75736 11.8284 6.75736Z" fill="orange" /></svg>

                            <div className=''>
                                <span>Pending Provisioning</span>
                                <br />
                                <h5>
                                    {nodes && nodes.filter(node => node.status === 'pendingprovisioning').length}
                                </h5>
                            </div>
                        </div>
                        {/* <div className='divider'></div> */}

                        <div className='status-data'>
                            <svg width="30px" height="30px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 13C16.5523 13 17 12.5523 17 12C17 11.4477 16.5523 11 16 11C15.4477 11 15 11.4477 15 12C15 12.5523 15.4477 13 16 13Z" fill="yellow" /><path d="M13 12C13 12.5523 12.5523 13 12 13C11.4477 13 11 12.5523 11 12C11 11.4477 11.4477 11 12 11C12.5523 11 13 11.4477 13 12Z" fill="yellow" /><path d="M8 13C8.55228 13 9 12.5523 9 12C9 11.4477 8.55228 11 8 11C7.44772 11 7 11.4477 7 12C7 12.5523 7.44772 13 8 13Z" fill="yellow" /><path fillRule="evenodd" clipRule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12Z" fill="yellow" /></svg>
                            <div className=''>
                                <span>Provisioning</span>
                                <br />

                                <h5>{nodes && nodes.filter(node => node.status === 'provisioning').length}</h5>
                            </div>
                        </div>
                        {/* <div className='divider'></div> */}


                        <div className='status-data'>
                            <svg width="30px" height="30px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 11C7.44772 11 7 11.4477 7 12C7 12.5523 7.44772 13 8 13H16C16.5523 13 17 12.5523 17 12C17 11.4477 16.5523 11 16 11H8Z" fill="orange" /><path fillRule="evenodd" clipRule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12Z" fill="orange" /></svg>

                            <div className=''>
                                <span>Pending Destroy</span>
                                <br />
                                <h5>
                                    {nodes && nodes.filter(node => node.status === 'pendingdestroy').length}
                                </h5>

                            </div>
                        </div>
                        {/* <div className='divider'></div> */}

                        <div className='status-data'>
                            <svg width="30px" height="30px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.70711 8.29289C9.31658 7.90237 8.68342 7.90237 8.29289 8.29289C7.90237 8.68342 7.90237 9.31658 8.29289 9.70711L10.5858 12L8.29289 14.2929C7.90237 14.6834 7.90237 15.3166 8.29289 15.7071C8.68342 16.0976 9.31658 16.0976 9.70711 15.7071L12 13.4142L14.2929 15.7071C14.6834 16.0976 15.3166 16.0976 15.7071 15.7071C16.0976 15.3166 16.0976 14.6834 15.7071 14.2929L13.4142 12L15.7071 9.70711C16.0976 9.31658 16.0976 8.68342 15.7071 8.29289C15.3166 7.90237 14.6834 7.90237 14.2929 8.29289L12 10.5858L9.70711 8.29289Z" fill="var(--color-red)" /><path fillRule="evenodd" clipRule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12Z" fill="var(--color-red)" /></svg>

                            <div className=''>
                                <span>Destroyed</span>
                                <br />
                                <h5>{nodes && nodes.filter(node => node.status === 'destroyed').length}</h5>
                            </div>
                        </div>

                    </div>

                    <div className='overview'>
                        <h5><b>Overview</b></h5>
                        <LineChart />
                    </div>
                    <div className='doughnut'>
                        <h5><b>Nodes per Use Case</b></h5>
                        <DonutChart />

                    </div>
                </div>

                <div className='table-filters'>
                    <input className='searchbar' type="text" placeholder='Search by uuid, email, date...' value={search} onChange={e => setSearch(e.target.value)} />
                    <div className='left'>
                        <p>Sort by</p>
                        <select
                            className="sort"
                            name="sortDate"
                            value={sortDate}
                            onChange={e => setSortDate(e.target.value)}
                        >
                            <option className='btn-sort' value='cd-desc'>Creation date (descending)</option>
                            <option className='btn-sort' value='cd-asc'>Creation date (ascending)</option>
                            <option className='btn-sort' value='ad-desc'>Activation date (descending)</option>
                            <option className='btn-sort' value='ad-asc'>Activation date (ascending)</option>
                            <option className='btn-sort' value='dd-desc'>Destroy date (descending)</option>
                            <option className='btn-sort' value='dd-asc'>Destroy date (ascending)</option>
                        </select>

                        <p>Filter by</p>
                        <select
                            name="statusFilter"
                            value={filterStatus}
                            onChange={e => setFilterStatus(e.target.value)}
                        >
                            <option value="">All Status</option>
                            <option value="pendingprovisioning">Pending Provisioning</option>
                            <option value="provisioning">Provisioning</option>
                            <option value="active">Active</option>
                            <option value="pendingdestroy">Pending Destroy</option>
                            <option value="destroyed">Destroyed</option>
                        </select>

                        <select
                            name="typeFilter"
                            value={filterType}
                            onChange={e => setFilterType(e.target.value)}
                        >
                            <option value="">All Types</option>
                            <option value="GPU">GPU</option>
                            <option value="CPU">CPU</option>
                        </select>



                    </div>
                </div>
                <div className='table'>
                    <table className='rounded-corners'>
                        <thead>
                            <tr>
                                <th>UUID</th>
                                <th>Email</th>
                                <th>Use case</th>
                                <th>Type</th>
                                <th>Creation</th>
                                <th>Activation</th>
                                <th>Destroy</th>
                                <th>Duration</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody ref={tbodyRef}>
                            {nodes && nodes.length > 0 ? (
                                nodes && nodes.filter(node => {
                                        // Filter by status
                                        if (filterStatus && node.status !== filterStatus) {
                                            return false;
                                        }

                                        // Filter by type
                                        if (filterType && node.type !== filterType) {
                                            return false;
                                        }

                                        // sort by date
                                        if (sortDate) {
                                            if (sortDate === 'cd-desc') {
                                                nodes.sort((a, b) => {
                                                    return new Date(b.creation_date) - new Date(a.creation_date);
                                                })
                                            }
                                            if (sortDate === 'cd-asc') {
                                                nodes.sort((a, b) => {
                                                    return new Date(a.creation_date) - new Date(b.creation_date);
                                                })
                                            }
                                            if (sortDate === 'ad-desc') {
                                                nodes.sort((a, b) => {
                                                    return new Date(b.activate_date) - new Date(a.activate_date);
                                                })
                                            }
                                            if (sortDate === 'ad-asc') {
                                                nodes.sort((a, b) => {
                                                    return new Date(a.activate_date) - new Date(b.activate_date);
                                                })
                                            }
                                            if (sortDate === 'dd-desc') {
                                                nodes.sort((a, b) => {
                                                    return new Date(b.destroy_date) - new Date(a.destroy_date);
                                                })
                                            }
                                            if (sortDate === 'dd-asc') {
                                                nodes.sort((a, b) => {
                                                    return new Date(a.destroy_date) - new Date(b.destroy_date);
                                                })
                                            }
                                        }

                                        // Filter by search
                                        if (search) {
                                            if (node?.uuid?.toLowerCase().indexOf(search.toLowerCase()) === -1 &&
                                                node?.email?.toLowerCase().indexOf(search.toLowerCase()) === -1) {
                                                return false;
                                            }
                                        }

                                        return true;

                                    })
                                    .map((node, index) => {
                                        return (
                                            <tr key={index}>

                                                <td className='uuid'>
                                                    <div className='tooltip'>
                                                        {node.uuid}
                                                    </div>
                                                    {
                                                        node.uuid.length > 8 ? node.uuid.slice(0, 8) + '...' : node.uuid
                                                    }
                                                </td>
                                                <td>{node.email}</td>
                                                <td>{node.usecase}</td>
                                                <td>{node.type}</td>
                                                <td>
                                                    {
                                                        node.creation_date?.split('T')[0].split('-').reverse().join('/')
                                                    }
                                                </td>
                                                <td>
                                                    {
                                                        node.activate_date?.split('T')[0].split('-').reverse().join('/')
                                                    }
                                                </td>
                                                <td>
                                                    {
                                                        node.destroy_date?.split('T')[0].split('-').reverse().join('/')
                                                    }
                                                </td>
                                                <td>
                                                    {node.duration}
                                                    {node.duration > 1 ? ' days' : ' day'}
                                                </td>
                                                <td className='status'>
                                                    <div className='tooltip'>
                                                        {
                                                            node.status === 'pendingprovisioning' ? (
                                                                'Pending provisioning'
                                                            ) : node.status === 'provisioning' ? (
                                                                'Provisioning'
                                                            ) : node.status === 'active' ? (
                                                                'Active'
                                                            ) : node.status === 'pendingdestroy' ? (
                                                                'Pending destroy'
                                                            ) : node.status === 'destroyed' ? (
                                                                'Destroyed'
                                                            ) : null
                                                        }
                                                    </div>
                                                    {
                                                        statusIcons.map((statusIcon, index) => {
                                                            if (statusIcon.status === node.status) {
                                                                return (
                                                                    <div key={index} dangerouslySetInnerHTML={{ __html: statusIcon.icon }} title={node.status} />
                                                                )
                                                            }
                                                            return null;
                                                        })
                                                    }
                                                </td>
                                                <td className='options'>{
                                                    node.status === 'active' ? (
                                                        <>
                                                            <button title='Edit' className='edit' onClick={() => { handleOpenPopupUpdate(); setUuidPopup(node.uuid) }}>
                                                                <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fillRule="evenodd" clipRule="evenodd" d="M13.1739 3.5968C13.8662 3.2047 14.686 3.10369 15.4528 3.31598C15.7928 3.41011 16.0833 3.57409 16.3571 3.7593C16.6172 3.9352 16.9155 4.16808 17.2613 4.43799L17.3117 4.47737C17.6575 4.74728 17.9559 4.98016 18.1897 5.18977C18.4358 5.41046 18.6654 5.65248 18.8393 5.95945C19.2314 6.65177 19.3324 7.47151 19.1201 8.23831C19.026 8.5783 18.862 8.86883 18.6768 9.14267C18.5009 9.40276 18.268 9.70112 17.998 10.0469L10.8953 19.1462C10.8773 19.1692 10.8596 19.1919 10.8421 19.2144C10.5087 19.6419 10.2566 19.9651 9.9445 20.2306C9.68036 20.4553 9.38811 20.6447 9.07512 20.794C8.70535 20.9704 8.30733 21.0685 7.78084 21.1983C7.75324 21.2051 7.72528 21.212 7.69696 21.219L5.57214 21.7435C5.42499 21.7799 5.25702 21.8215 5.10885 21.8442C4.94367 21.8696 4.68789 21.8926 4.40539 21.8022C4.06579 21.6934 3.77603 21.4672 3.58809 21.1642C3.43175 20.9121 3.39197 20.6584 3.3765 20.492C3.36262 20.3427 3.36213 20.1697 3.3617 20.0181C3.36167 20.0087 3.36165 19.9994 3.36162 19.9902L3.35475 17.8295C3.35465 17.8003 3.35455 17.7715 3.35445 17.7431C3.3525 17.2009 3.35103 16.7909 3.4324 16.3894C3.50128 16.0495 3.61406 15.72 3.76791 15.4093C3.94967 15.0421 4.20204 14.7191 4.53586 14.2918C4.55336 14.2694 4.57109 14.2467 4.58905 14.2237L11.6918 5.12435C11.9617 4.77856 12.1946 4.48019 12.4042 4.2464C12.6249 4.00025 12.8669 3.77065 13.1739 3.5968ZM14.9191 5.24347C14.6635 5.17271 14.3903 5.20638 14.1595 5.33708C14.1203 5.35928 14.0459 5.41135 13.8934 5.5815C13.7348 5.75836 13.5438 6.00211 13.2487 6.38018L16.4018 8.84145C16.697 8.46338 16.887 8.21896 17.0201 8.02221C17.1482 7.83291 17.1806 7.74808 17.1926 7.70467C17.2634 7.44907 17.2297 7.17583 17.099 6.94505C17.0768 6.90586 17.0247 6.83145 16.8546 6.6789C16.6777 6.52033 16.434 6.32938 16.0559 6.03426C15.6778 5.73914 15.4334 5.54904 15.2367 5.41597C15.0474 5.28794 14.9625 5.25549 14.9191 5.24347ZM15.1712 10.418L12.0181 7.95674L6.16561 15.4543C5.75585 15.9792 5.6403 16.135 5.56031 16.2966C5.48339 16.452 5.42699 16.6167 5.39256 16.7866C5.35675 16.9633 5.35262 17.1572 5.35474 17.8231L5.36082 19.7357L7.2176 19.2773C7.86411 19.1177 8.05119 19.0666 8.21391 18.9889C8.37041 18.9143 8.51653 18.8196 8.64861 18.7072C8.78593 18.5904 8.90897 18.4405 9.31872 17.9156L15.1712 10.418ZM12 21C12 20.4477 12.4477 20 13 20H20C20.5523 20 21 20.4477 21 21C21 21.5523 20.5523 22 20 22H13C12.4477 22 12 21.5523 12 21Z" fill="#fff" />
                                                                </svg>
                                                            </button>
                                                            <button title='Destroy' className='destroy' onClick={() => { handleOpenPopup(); setUuidPopup(node.uuid) }}>
                                                                <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fillRule="evenodd" clipRule="evenodd" d="M7.10002 5H3C2.44772 5 2 5.44772 2 6C2 6.55228 2.44772 7 3 7H4.06055L4.88474 20.1871C4.98356 21.7682 6.29471 23 7.8789 23H16.1211C17.7053 23 19.0164 21.7682 19.1153 20.1871L19.9395 7H21C21.5523 7 22 6.55228 22 6C22 5.44772 21.5523 5 21 5H19.0073C19.0018 4.99995 18.9963 4.99995 18.9908 5H16.9C16.4367 2.71776 14.419 1 12 1C9.58104 1 7.56329 2.71776 7.10002 5ZM9.17071 5H14.8293C14.4175 3.83481 13.3062 3 12 3C10.6938 3 9.58254 3.83481 9.17071 5ZM17.9355 7H6.06445L6.88085 20.0624C6.91379 20.5894 7.35084 21 7.8789 21H16.1211C16.6492 21 17.0862 20.5894 17.1192 20.0624L17.9355 7ZM14.279 10.0097C14.83 10.0483 15.2454 10.5261 15.2068 11.0771L14.7883 17.0624C14.7498 17.6134 14.2719 18.0288 13.721 17.9903C13.17 17.9517 12.7546 17.4739 12.7932 16.9229L13.2117 10.9376C13.2502 10.3866 13.7281 9.97122 14.279 10.0097ZM9.721 10.0098C10.2719 9.97125 10.7498 10.3866 10.7883 10.9376L11.2069 16.923C11.2454 17.4739 10.83 17.9518 10.2791 17.9903C9.72811 18.0288 9.25026 17.6134 9.21173 17.0625L8.79319 11.0771C8.75467 10.5262 9.17006 10.0483 9.721 10.0098Z" fill="#fff" />
                                                                </svg>
                                                            </button>
                                                        </>
                                                    ) : (
                                                        <>
                                                            <button className="edit" disabled>
                                                                <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fillRule="evenodd" clipRule="evenodd" d="M13.1739 3.5968C13.8662 3.2047 14.686 3.10369 15.4528 3.31598C15.7928 3.41011 16.0833 3.57409 16.3571 3.7593C16.6172 3.9352 16.9155 4.16808 17.2613 4.43799L17.3117 4.47737C17.6575 4.74728 17.9559 4.98016 18.1897 5.18977C18.4358 5.41046 18.6654 5.65248 18.8393 5.95945C19.2314 6.65177 19.3324 7.47151 19.1201 8.23831C19.026 8.5783 18.862 8.86883 18.6768 9.14267C18.5009 9.40276 18.268 9.70112 17.998 10.0469L10.8953 19.1462C10.8773 19.1692 10.8596 19.1919 10.8421 19.2144C10.5087 19.6419 10.2566 19.9651 9.9445 20.2306C9.68036 20.4553 9.38811 20.6447 9.07512 20.794C8.70535 20.9704 8.30733 21.0685 7.78084 21.1983C7.75324 21.2051 7.72528 21.212 7.69696 21.219L5.57214 21.7435C5.42499 21.7799 5.25702 21.8215 5.10885 21.8442C4.94367 21.8696 4.68789 21.8926 4.40539 21.8022C4.06579 21.6934 3.77603 21.4672 3.58809 21.1642C3.43175 20.9121 3.39197 20.6584 3.3765 20.492C3.36262 20.3427 3.36213 20.1697 3.3617 20.0181C3.36167 20.0087 3.36165 19.9994 3.36162 19.9902L3.35475 17.8295C3.35465 17.8003 3.35455 17.7715 3.35445 17.7431C3.3525 17.2009 3.35103 16.7909 3.4324 16.3894C3.50128 16.0495 3.61406 15.72 3.76791 15.4093C3.94967 15.0421 4.20204 14.7191 4.53586 14.2918C4.55336 14.2694 4.57109 14.2467 4.58905 14.2237L11.6918 5.12435C11.9617 4.77856 12.1946 4.48019 12.4042 4.2464C12.6249 4.00025 12.8669 3.77065 13.1739 3.5968ZM14.9191 5.24347C14.6635 5.17271 14.3903 5.20638 14.1595 5.33708C14.1203 5.35928 14.0459 5.41135 13.8934 5.5815C13.7348 5.75836 13.5438 6.00211 13.2487 6.38018L16.4018 8.84145C16.697 8.46338 16.887 8.21896 17.0201 8.02221C17.1482 7.83291 17.1806 7.74808 17.1926 7.70467C17.2634 7.44907 17.2297 7.17583 17.099 6.94505C17.0768 6.90586 17.0247 6.83145 16.8546 6.6789C16.6777 6.52033 16.434 6.32938 16.0559 6.03426C15.6778 5.73914 15.4334 5.54904 15.2367 5.41597C15.0474 5.28794 14.9625 5.25549 14.9191 5.24347ZM15.1712 10.418L12.0181 7.95674L6.16561 15.4543C5.75585 15.9792 5.6403 16.135 5.56031 16.2966C5.48339 16.452 5.42699 16.6167 5.39256 16.7866C5.35675 16.9633 5.35262 17.1572 5.35474 17.8231L5.36082 19.7357L7.2176 19.2773C7.86411 19.1177 8.05119 19.0666 8.21391 18.9889C8.37041 18.9143 8.51653 18.8196 8.64861 18.7072C8.78593 18.5904 8.90897 18.4405 9.31872 17.9156L15.1712 10.418ZM12 21C12 20.4477 12.4477 20 13 20H20C20.5523 20 21 20.4477 21 21C21 21.5523 20.5523 22 20 22H13C12.4477 22 12 21.5523 12 21Z" fill="#fff" />
                                                                </svg>
                                                            </button>
                                                            <button className='destroy' disabled>
                                                                <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fillRule="evenodd" clipRule="evenodd" d="M7.10002 5H3C2.44772 5 2 5.44772 2 6C2 6.55228 2.44772 7 3 7H4.06055L4.88474 20.1871C4.98356 21.7682 6.29471 23 7.8789 23H16.1211C17.7053 23 19.0164 21.7682 19.1153 20.1871L19.9395 7H21C21.5523 7 22 6.55228 22 6C22 5.44772 21.5523 5 21 5H19.0073C19.0018 4.99995 18.9963 4.99995 18.9908 5H16.9C16.4367 2.71776 14.419 1 12 1C9.58104 1 7.56329 2.71776 7.10002 5ZM9.17071 5H14.8293C14.4175 3.83481 13.3062 3 12 3C10.6938 3 9.58254 3.83481 9.17071 5ZM17.9355 7H6.06445L6.88085 20.0624C6.91379 20.5894 7.35084 21 7.8789 21H16.1211C16.6492 21 17.0862 20.5894 17.1192 20.0624L17.9355 7ZM14.279 10.0097C14.83 10.0483 15.2454 10.5261 15.2068 11.0771L14.7883 17.0624C14.7498 17.6134 14.2719 18.0288 13.721 17.9903C13.17 17.9517 12.7546 17.4739 12.7932 16.9229L13.2117 10.9376C13.2502 10.3866 13.7281 9.97122 14.279 10.0097ZM9.721 10.0098C10.2719 9.97125 10.7498 10.3866 10.7883 10.9376L11.2069 16.923C11.2454 17.4739 10.83 17.9518 10.2791 17.9903C9.72811 18.0288 9.25026 17.6134 9.21173 17.0625L8.79319 11.0771C8.75467 10.5262 9.17006 10.0483 9.721 10.0098Z" fill="#fff" />
                                                                </svg>
                                                            </button>
                                                        </>
                                                    )
                                                }</td>
                                            </tr>
                                        )
                                    })
                            ) : (
                                <tr id='notfound'>
                                    <td colSpan="10">No node found</td>
                                </tr>
                            )}
                            {
                                tbodyEmpty === true && (
                                    <tr id='notfound'>
                                        <td colSpan="10">No node found</td>
                                    </tr>
                                )
                            }
                        </tbody>

                    </table>

                </div>

            </div>
            <div className="popup" ref={popupRef}>
                <div className="popup-content" ref={popupContentRef}>
                    <div className="popup-header">
                        {/* <img src={trashWhite} alt='trash'></img> */}
                        <h4>Destroy a node is irreversible.</h4>
                    </div>
                    <div className="popup-body">
                        <p>Are you sure you want to destroy node<br /><span>{uuidPopup}</span> ?</p>
                    </div>
                    <form onSubmit={handleSubmitDestroy}>
                        <div>
                            <label htmlFor="verify">Type 'destroy' to confirm :</label>
                            <input type="text" name="verify" id='verify' placeholder="destroy" onChange={(event) => {
                                if (event.target.value === 'destroy') {
                                    btnDeleteRef.current.disabled = false;
                                } else {
                                    btnDeleteRef.current.disabled = true;
                                }
                            }} />
                        </div>
                        <div className="popup-footer">
                            <button className="btn btn-cancel" onClick={handleClosePopup} type="button">
                                Cancel
                            </button>
                            <button className="btn btn-delete" disabled={true} type="submit" ref={btnDeleteRef}>
                                <span>Destroy</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>



            <div className="popup" ref={popupRefUpdate}>
                <div className="popup-content" ref={popupContentRefUpdate}>
                    <div className="popup-header">
                        <h4>Edit node</h4>
                    </div>
                    <form className="node_form" onSubmit={handleSubmitUpdate}>
                        <label>
                            Add duration (days additonate to the current duration) for node<br /><span>{uuidPopup}</span>
                        </label>

                        <select name="duration">
                            <option value="1">1 day</option>
                            <option value="2">2 days</option>
                            <option value="3">3 days</option>
                            <option value="4">4 days</option>
                            <option value="5">5 days</option>
                            <option value="6">6 days</option>
                            <option value="7">7 days</option>
                        </select>
                        <div className="popup-footer">
                            <button className="btn btn-cancel" onClick={handleClosePopupUpdate} type="button">
                                Cancel
                            </button>
                            <input className="btn btn-update" type="submit" value="Submit" />
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}