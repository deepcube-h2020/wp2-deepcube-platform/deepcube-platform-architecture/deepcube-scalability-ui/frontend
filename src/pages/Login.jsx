import { useEffect, useContext } from 'react';
import { useNavigate } from "react-router-dom";
import { AuthContext } from '../context/AuthContext';
import { Loading } from '../components/Loading';

export const Login = () => {

    const navigate = useNavigate();
    const { logged, login } = useContext(AuthContext);

    useEffect(() => {
        if (logged) {
            navigate('/');
        }
    }, [logged, navigate]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        const password = data.get('password');

        login(password);
    }

    return (
        <>
            <Loading />
            <div className="login">
                <img draggable="false" src="./images/hopsworks.png" alt="hopsworks logo" />
                <form onSubmit={handleSubmit}>
                    <input type="password" name="password" id="password" placeholder="Password" />
                    <input type="submit" value="Login" />
                </form>
            </div>
        </>
    )
}