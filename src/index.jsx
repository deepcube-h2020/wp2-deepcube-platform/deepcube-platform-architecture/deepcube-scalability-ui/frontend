import React from 'react'; import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";

import { AuthContextProvider } from './context/AuthContext';
import { NodesContextProvider } from './context/NodesContext';

import './index.css';
import { Login } from './pages/Login';
import { Dashboard } from './pages/Dashboard';
import { Add } from './pages/Add';
import { NotFound } from './pages/NotFound';

const router = createBrowserRouter(
  createRoutesFromElements([
    <Route path="/" element={<Dashboard />} />,
    <Route path="/login" element={<Login />} />,
    <Route path="/add" element={<Add />} />,
    <Route path="*" element={<NotFound />} />,
  ])
);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <AuthContextProvider>
      <NodesContextProvider>
        <RouterProvider router={router} />
      </NodesContextProvider>
    </AuthContextProvider>
  </React.StrictMode>
);
