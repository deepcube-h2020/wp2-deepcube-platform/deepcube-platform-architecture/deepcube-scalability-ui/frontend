//authentification pas tres secure mais suffisant pour le projet
import React, { createContext, useState, useEffect } from 'react';
import { addLocalStorage, getLocalStorage, removeLocalStorage } from '../utils/localStorageUtils'
import env from "react-dotenv";

const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {

    const API_URL = env.API_URL;
    const API_PORT = env.API_PORT;

    const [logged, setLogged] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const token = getLocalStorage('password');
        if (token) {
            fetch(API_URL + ':' + API_PORT + '/api/v1/auth/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: new URLSearchParams({
                    password: token
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data.error) {
                        console.log('Error:', data.error);
                    }
                    if (data.message === 'success') {
                        setLogged(true);
                    }
                    setTimeout(() => {
                        setLoading(false);
                    }, 1000);
                })
                .catch((error) => {
                    console.log('Error:', error);
                });
        } else {
            setTimeout(() => {
                setLoading(false);
            }, 1000);
        }
    }, []);

    const login = (password) => {
        fetch(API_URL + ':' + API_PORT + '/api/v1/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: new URLSearchParams({
                password: password
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                if (data.message === 'success') {
                    setLogged(true);
                    addLocalStorage('password', password);
                }
            })
            .catch((error) => {
                console.log('Error:', error);
                alert(error);
            });
    }

    const logout = () => {
        removeLocalStorage('password');
        setLogged(false);
    }

    return (
        <AuthContext.Provider value={{ logged, login, logout, loading }}>
            {children}
        </AuthContext.Provider>
    );
}

export { AuthContext, AuthContextProvider };