import React, { createContext, useState, useEffect } from 'react';
import env from "react-dotenv";

const NodesContext = createContext();

const NodesContextProvider = ({ children }) => {

    const API_URL = env.API_URL;
    const API_PORT = env.API_PORT;

    const [nodes, setNodes] = useState([]);

    const getNodes = () => {
        fetch(API_URL + ':' + API_PORT + '/api/v1/node/list')
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    console.log('Error:', data.error);
                    return;
                }
                setNodes(data.nodes);
            })
            .catch((error) => {
                console.log('Error:', error);
                // alert(error);
            });
    }

    useEffect(() => {
        getNodes();
    }, []);

    return (
        <NodesContext.Provider value={{ nodes, getNodes }}>
            {children}
        </NodesContext.Provider>
    );
}

export { NodesContext, NodesContextProvider };