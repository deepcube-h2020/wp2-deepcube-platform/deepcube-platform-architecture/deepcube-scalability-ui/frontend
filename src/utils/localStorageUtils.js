const addLocalStorage = (key, value) => {
    window.localStorage.setItem(key, value);
};

const getLocalStorage = (key) => {
    return window.localStorage.getItem(key);
};

const removeLocalStorage = (key) => {
    window.localStorage.removeItem(key);
};

const clearLocalStorage = () => {
    window.localStorage.clear();
};

export { addLocalStorage, getLocalStorage, removeLocalStorage, clearLocalStorage };
