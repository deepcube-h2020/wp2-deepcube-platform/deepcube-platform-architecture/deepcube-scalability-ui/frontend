import { AuthContext } from '../context/AuthContext';
import { useContext } from 'react';

export const Loading = () => {
    const { loading } = useContext(AuthContext);
    
    return (
        <>
            {loading &&
                <div className="loading">
                    <img draggable="false" src="./images/hopsworks.png" alt="hopsworks logo" />
                    <div className="spinner"></div>
                </div>
            }
        </>
    )
}
