import React, { useContext, useMemo } from 'react';
import { NodesContext } from '../context/NodesContext';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export const options = {
    responsive: true,
    interaction: {
        mode: 'index',
        //   mode: 'index' as const,
        intersect: false,
    },
    stacked: false,
    plugins: {
        title: {
            display: false,
       },
    },
    scales: {
        y: {
            type: 'linear',
            display: true,
            position: 'left',
        },
        y1: {
            type: 'linear',
            display: true,
            position: 'right',
            grid: {
                drawOnChartArea: false,
            },
        },
    },
};

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export const LineChart = () => {
    const { nodes } = useContext(NodesContext);

    const data = useMemo(() => ({
        labels,
        datasets: [
            {
                label: 'UC1',
                data: labels.map(() => Math.random() * 100),
                borderColor: 'rgba(200, 247, 197, 1)',
                backgroundColor: 'rgba(200, 247, 197, 0.2)',
                yAxisID: 'y',
                borderWidth: 1,

            },
            {
                label: 'UC2',
                data: labels.map(() => Math.random() * 100),
                borderColor: 'rgb(195, 255, 104)',
                backgroundColor: 'rgba(195, 255, 104, 0.2)',
                yAxisID: 'y1',
                borderWidth: 1,

            },
            {
                label: 'UC3',
                data: labels.map(() => Math.random() * 100),
                borderColor: 'rgb(102, 204, 153)',
                backgroundColor: 'rgba(102, 204, 153, 0.2)',
                yAxisID: 'y1',
                borderWidth: 1,

            },
            {
                label: 'UC4A',
                data: labels.map(() => Math.random() * 100),
                borderColor: 'rgb(3, 201, 169)',
                backgroundColor: 'rgba(3, 201, 169, 0.2)',
                yAxisID: 'y1',
                borderWidth: 1,

            },
            {
                label: 'UC4B',
                data: labels.map(() => Math.random() * 100),
                borderColor: 'rgb(13, 180, 185)',
                backgroundColor: 'rgba(13, 180, 185, 0.2)',
                yAxisID: 'y1',
                borderWidth: 1,

            },
            {
                label: 'UC5',
                data: labels.map(() => Math.random() * 100),
                borderColor: 'rgb(0, 83, 255)',
                backgroundColor: 'rgba(0, 83, 255, 0.2)',
                yAxisID: 'y1',
                borderWidth: 1,

            },
        
        ],
    }), []);
        // labels,
        // datasets: [
        //     {
        //         label: 'GPU',
        //         data: nodes.map(node => node.type === 'GPU' ? node.gpu : 0),
        //         borderColor: 'rgb(255, 99, 132)',
        //         backgroundColor: 'rgba(255, 99, 132, 0.5)',
        //     },
        //     {
        //         label: 'CPU',
        //         data: nodes.map(node => node.type === 'CPU' ? node.cpu : 0),
        //         borderColor: 'rgb(53, 162, 235)',
        //         backgroundColor: 'rgba(53, 162, 235, 0.5)',
        //     },
        // ],
        // }), [nodes]);

    // }));


    return (
        <Line options={options} data={data} />
    )
}