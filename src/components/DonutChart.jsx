import React, { useContext, useMemo } from 'react';
import { NodesContext } from '../context/NodesContext';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';

ChartJS.register(ArcElement, Tooltip, Legend);

export const DonutChart = () => {
    const { nodes } = useContext(NodesContext);

    const data = {
        labels: ['UC1', 'UC2', 'UC3', 'UC4A', 'UC4B', 'UC5'],
        datasets: [
            {
                label: 'Amount of nodes',
                data: [nodes && nodes.filter(node => node.usecase === 'UC1').length, nodes && nodes.filter(node => node.usecase === 'UC2').length, nodes && nodes.filter(node => node.usecase === 'UC3').length, nodes && nodes.filter(node => node.usecase === 'UC4a').length, nodes && nodes.filter(node => node.usecase === 'UC4b').length, nodes && nodes.filter(node => node.usecase === 'UC5').length],
                backgroundColor: [
                    'rgba(200, 247, 197, 0.2)',
                    'rgba(195, 255, 104, 0.2)',
                    'rgba(102, 204, 153, 0.2)',
                    'rgba(3, 201, 169, 0.2)',
                    'rgba(13, 180, 185, 0.2)',
                    'rgba(0, 83, 255, 0.2)',
                ],
                borderColor: [
                    'rgba(200, 247, 197, 1)',
                    'rgba(195, 255, 104, 1)',
                    'rgba(102, 204, 153, 1)',
                    'rgba(3, 201, 169, 1)',
                    'rgba(13, 180, 185, 1)',
                    'rgba(0, 83, 255, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };


    return (<Doughnut data={data} />);


}